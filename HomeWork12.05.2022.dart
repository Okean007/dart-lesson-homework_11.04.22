void main(List<String> args) {
  List total = [1, 2, 3, 4, 5, 6];
  bool check = true;
  int sum = 0;
  funcExersice1(a: 100);
  funcExersice2(a: 10, b: 15);
  print(funcExersice3(a: 1000, b: 200, c: 4));
  funcExersice4(min: 60);
  print(funcExersice5(a: total));
  print(funcExercise6(a: check));
  print(funcExersice7(a: sum));
  funcExersice7_2(a: sum);
  funExercise8();
}

int funcExersice1({required int a}) {
  int sum = a * a;
  print(sum);
  return sum;
}

int funcExersice2({required int a, required int b}) {
  int sum = a + b;
  print(sum);
  return sum;
}

double funcExersice3({required int a, required int b, required int c}) =>
    (a - b) / c;

String funcExersice4({required int min}) {
  String sum = 'minute $min = ${min * 60} seconds ';
  print(sum);
  return sum;
}

int funcExersice5({required List a}) {
  return a[1];
}

String funcExercise6({required bool a}) {
  if (a == true) {
    return ('Правда');
  } else {
    return ('Ложь');
  }
}

bool funcExersice7({required int a}) {
  if (a <= 0) {
    return true;
  } else {
    return false;
  }
}

void funcExersice7_2({required int a}) {
  if (a <= 0) {
    print(true);
  } else {
    print(false);
  }
}

funExercise8() {
  List<int> myList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9];

  int myVariable = 0;
  bool isContains = false;

  myList.forEach((e) {
    if (myVariable == e) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Да');
  } else {
    print('Нет');
  }
}
